//
//  ViewController.swift
//  FrasesDoDia
//
//  Created by Anselmo Lira on 19/01/18.
//  Copyright © 2018 Anselmo Lira. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var dailySentence: UILabel!
    @IBAction func btnNewSentence(_ sender: Any)
    {
        // Montando o array de frases
        var sentences: [String] = []
        sentences.append("Se você traçar metas absurdamente altas e falhar, seu fracasso será muito melhor que o sucesso de todos.")
        sentences.append("Ter sucesso é falhar repetidamente, mas sem perder o entusiasmo.")
        sentences.append("Não é o mais forte que sobrevive, nem o mais inteligente. Quem sobrevive é o mais disposto à mudança.")
        sentences.append("A vingança nunca é plena. Mata a alma e a envenena.")
        
        // Gerando aleatoriamente o índice da frase a ser exibida
        let sentenceIndex: UInt32 = arc4random_uniform(4)
        // Exibindo a frase na Label
        dailySentence.text = sentences[Int(sentenceIndex)]
        
        /*
         var -> cria variável, que permite ler e escrever
         let -> cria constante, cujo valor nunca muda
        */
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
