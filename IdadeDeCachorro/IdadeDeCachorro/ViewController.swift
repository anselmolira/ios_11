//
//  ViewController.swift
//  IdadeDeCachorro
//
//  Created by Anselmo Lira on 17/01/2018.
//  Copyright © 2018 Anselmo Lira. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var dogAgeField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func ageCalculate(_ sender: Any)
    {
        if dogAgeField.text == ""
        {
            resultLabel.text = "Forneca uma idade válida"
        }
        else
        {
            let dogAge = Int(dogAgeField.text!)!
            if dogAge < 1 || dogAge > 30
            {
                resultLabel.text = "Forneça uma idade entre 1 e 30"
            }
            else
            {
                let age = dogAge * 7
                resultLabel.text = "A idade do cachorro é: " + String(age)
            }
        }
    }
    
    override func viewDidLoad()
    {
        // Este método é executado sempre que a tela é carregada
        super.viewDidLoad()
        print("App Idade de Cachorro!!");
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
