//
//  ViewController.swift
//  Sorteio
//
//  Created by Anselmo Lira on 19/01/18.
//  Copyright © 2018 Anselmo Lira. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var btnNumGen: UIButton!
    @IBOutlet weak var resultLegend: UILabel!
    var numGenHistory: [UInt32] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func btnNumGenClick(_ sender: Any)
    {
        var randomNum = arc4random_uniform(11)
        // Exibindo o número na tela
        resultLegend.text = String(randomNum)
        // Guardando o histórico de números
        numGenHistory.append(randomNum)
        
        // Log de cliques no botão
        print("Cliquei no botão")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
